<?php

/*

Plugin Name: AOA Scrolldepth
Description: Wrapper for Scroll Depth, a Google Analytics tool for measuring page scrolling. See https://github.com/robflaherty/jquery-scrolldepth
Version: 0.1.0
Author: Patrick Sinco and Rob Flaherty

*/

if ( ! defined( 'WPINC' ) ) {
  die;
}

function elit_sd_enqueue_scripts() {
  $path = 'lib/jquery.scrolldepth.min.js';

  if ( ! is_admin() ) {
    wp_enqueue_script(
      'scrollDepth',
      plugins_url( $path, __FILE__ ),
      array( 'jquery' ),
      filemtime( plugin_dir_path(__FILE__) . '/' . $path ),
      true
    );

    $output  = 'jQuery.scrollDepth({' . PHP_EOL;
    $output .= '  minHeight: 2000,' . PHP_EOL;
    $output .= '  pixelDepth: false,' . PHP_EOL;
    $output .= '  elements: [\'.shareWrapper\'],' . PHP_EOL;
    $output .= '  userTiming: true,' . PHP_EOL;
    $output .= "});";

    wp_add_inline_script( 'scrollDepth', $output, 'after' );
  }
}
add_action( 'wp_enqueue_scripts' , 'elit_sd_enqueue_scripts' );
