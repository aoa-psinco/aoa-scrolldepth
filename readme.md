### AOA Scroll Depth
WordPress plugin that's a wrapper for Scroll Depth, a Google Analytics tool for measuring page scrolling. 

See [https://github.com/robflaherty/jquery-scrolldepth](https://github.com/robflaherty/jquery-scrolldepth)
